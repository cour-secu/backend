module.exports = Subcription = () => {
    const DB = require('../connector')();

    return {
        get: (id, cb) => {
            let sql = "SELECT * FROM subscription WHERE subscription_id = ?;";
            DB.exec(sql, [id], cb);
        },
        getAll: (cb) => {
            let sql = "SELECT * FROM subscription;";
            DB.exec(sql, [], cb);
        },
        add: (obj, cb) => {
            let sql = "INSERT INTO subscription(`label`, `price`, `credit`) VALUES"
                + '("?",?,"?");';
            DB.exec(sql,[obj.label, obj.price, obj.credit], cb);
        },
        update: (obj, cb) => {
            let sql = 'UPDATE subscription SET `label` = "?", SET `price` = ?, SET `credit` = "?"; WHERE subscription_id = "?";';
            DB.exec(sql, [obj.label, obj.price, obj.credit], cb);
        },
        remove: (id, cb) => {
            let sql = 'DELETE FROM subscription'
                + 'WHERE subscription_id = ?;';
            db.exec(sql, [id], cb);
        },
    }
};