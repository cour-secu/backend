module.exports = HistProgram = () => {
    const DB = require('../connector')();

    return {
        get: (id, cb) => {
            let sql = "SELECT * FROM hist_program WHERE hist_program_id = ?;";
            DB.exec(sql, [id], cb);
        },
        getAll: (cb) => {
            let sql = "SELECT * FROM hist_program;";
            DB.exec(sql, [], cb);
        },
        add: (obj, cb) => {
            let sql = "INSERT INTO hist_program(`client_id`, `program_id`, `wash_date`, `wash_price`, `payment_method`) VALUES"
                    + '(?, ?, ?, ?, "?");';
            DB.exec(sql, [obj.client_id, obj.program_id, obj.wash_date, obj.price, obj.payment_method], cb);
        },
        remove: (id, cb) => {
            let sql = 'DELETE FROM program WHERE program_id = ?;';
            DB.exec(sql, [id], cb);
        }
    }
}
