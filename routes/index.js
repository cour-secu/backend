const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Program = require('../database/models/program')();
const Subscription = require('../database/models/subscription')();
const Client = require('../database/models/client')();

const app = express();

function renameKey(key) {
    let newKey = key.split('_').map(sub => sub.charAt(0).toUpperCase() + sub.substr(1)).join('');
    return newKey = newKey.charAt(0).toLowerCase() + newKey.substr(1);
}
function replaceAttribute(item) {
    let newItem = {};
    for (const attr in item) {
        console.log(attr);
        if (item.hasOwnProperty(attr)) {
            const newAttr = renameKey(attr);
            item[newAttr] = item[attr];
            newItem = Object.assign(newItem, {[newAttr]: item[attr]});
            console.log(newItem);
        }
    }
    return newItem;
}
function remapResult(result) {
    if (Array.isArray(result)) {
        console.log(result);
        result = Object.assign(result.map(item => {
           return replaceAttribute(item)
        }));
    } else {
        replaceAttribute(result);
    }
    return result;
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/* GET home page. */
router.get('/', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});

// SUBSCRIPTIONS
router.get('/subscriptions', function(req, res, next) {
    Subscription.getAll((err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.get('/subscriptions/:id', function(req, res, next) {
    Subscription.get(req.params.id, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.post('/subscriptions', function(req, res, next) {
    if (!req.body) return res.sendStatus(400);
    Subscription.add(req.body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({subscriptionId: result.insertId}));
    });
});
router.put('/subscriptions/:id', function(req, res, next) {
    if (!req.body) return res.sendStatus(400);
    const body = Object.assign(req.body, {id: req.params.id});
    Subscription.update(body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({subscriptionId: req.params.id}));
    });
});
router.delete('/subscriptions/:id', function(req, res, next) {
    Subscription.remove(req.params.id, (err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({subscriptionId: req.params.id}));
    });
});

// PROGRAMS
router.get('/programs', function(req, res, next) {
    Program.getAll((err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.get('/programs/:id', function(req, res, next) {
    Program.get(req.params.id, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.post('/programs', function(req, res, next) {
    if (!req.body) return res.sendStatus(400);
    Program.add(req.body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({programId: result.insertId}));
    });
});
router.put('/programs/:id', function(req, res, next) {
    console.log(req.params);
    if (!req.body) return res.sendStatus(400);
    const body = Object.assign(req.body, {id: req.params.id});
    Program.update(body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({programId: req.params.id}));
    });
});
router.delete('/programs/:id', function(req, res, next) {
    Program.remove(req.params.id, (err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({programId: req.params.id}));
    });
});

// PROGRAMS HISTORIC
router.get('/historic/programs', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.post('/historic/programs', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.put('/historic/programs/:id', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.delete('/historic/programs/:id', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});

// SUBSCRIPTIONS HISTORIC
router.get('/historic/subscriptions', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.post('/historic/subscriptions', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.put('/historic/subscriptions/:id', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});
router.delete('/historic/subscriptions/:id', function(req, res, next) {
    const result = [];
    res.setHeader('Content-Type', 'application/json');
    res.send(remapResult(result));
});

// CLIENT
router.get('/clients', function(req, res, next) {
    Client.getAll((err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.get('/clients/:id', function(req, res, next) {
    Client.get(req.params.id, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult(result));
    });
});
router.post('/clients', function(req, res, next) {
    if (!req.body) return res.sendStatus(400);
    Client.add(req.body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({clientId: result.insertId}));
    });
});
router.put('/clients/:id', function(req, res, next) {
    if (!req.body) return res.sendStatus(400);
    const body = Object.assign(req.body, {id: req.params.id});
    Client.update(body, (err, result) => {
      if (err) return res.sendStatus(400);
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({clientId: req.params.id}));
    });
});
router.delete('/clients/:id', function(req, res, next) {
    Client.remove(req.params.id, (err, result) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(remapResult({clientId: req.params.id}));
    });
});

router.get('/404', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send({'error': 'requested route does not exist'});
});

router.use(function(req, res) {
    res.redirect('/404');
});

module.exports = router;
