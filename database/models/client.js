module.exports = Client = () => {
    const DB = require('../connector')();

  return  {
    get: (id, cb) => {
      let sql = "SELECT * FROM client WHERE client_id = ?;"
      DB.exec(sql,[id], cb);
    },
    getAll: (cb) => {
      let sql = "SELECT * FROM client;"
      DB.exec(sql,[], cb);
    },
    add: (obj, cb) => {
      let sql = "INSERT INTO client(`last_name`, `first_name`, `date_of_birth`, `address`, `zip_code`, `city`, `country`, `credit`) VALUES"
              + '("?", "?", "?","?","?","?","?",?);'
      DB.exec(sql, [obj.lastName, obj.firstName, obj.birth, obj.address, obj.cp, obj.city, obj.country, obj.credit], cb);
    },
    update: (obj, cb) => {
      let sql = 'UPDATE client'
      + ' SET `last_name` = "?", `first_name` = "?", `date_of_birth` = "?", `address` = "?", `zip_code` = "?", `city` = "?", `country` = "?", `credit` = ?'
      + ' WHERE client_id = ?;'
      DB.exec(sql, [obj.lastName, obj.firstName, obj.birth, obj.address, obj.cp, obj.city, obj.country, obj.credit, obj.id], cb);
    },
    remove: (id, cb) => {
      let sql = 'DELETE FROM client'
      + ' WHERE client_id = ?;'
      DB.exec(sql, [id], cb);
    }
  }
};