USE eatcarwash;

ALTER TABLE hist_program
DROP FOREIGN KEY FK_client_pgrm,
DROP FOREIGN KEY FK_program_pgrm;

ALTER TABLE hist_subscription
DROP FOREIGN KEY FK_client_sub,
DROP FOREIGN KEY FK_subscription_sub;

TRUNCATE TABLE hist_program;
TRUNCATE TABLE hist_subscription;
TRUNCATE TABLE client;
TRUNCATE TABLE program;
TRUNCATE TABLE subscription;

ALTER TABLE hist_program
ADD CONSTRAINT FK_client_pgrm
    FOREIGN KEY (client_id) REFERENCES client(client_id),
ADD CONSTRAINT FK_program_pgrm
    FOREIGN KEY (program_id) REFERENCES program(program_id);

ALTER TABLE hist_subscription
ADD CONSTRAINT FK_client_sub
    FOREIGN KEY (client_id) REFERENCES client(client_id),
ADD CONSTRAINT FK_subscription_sub
    FOREIGN KEY (subscription_id) REFERENCES subscription(subscription_id);

INSERT INTO program (`label`, `duration`, `price`)VALUES
("Lavage 1", 60, 1),
("Lavage 2", 120, 2),
("Lavage 3", 180, 3),
("Lavage 4", 240, 4),
("Lavage 5", 300, 5);

INSERT INTO subscription (`label`, `price`, `credit`)VALUES
("Forfait 1", 20, 25),
("Forfait 2", 40, 50),
("Forfait 3", 60, 75),
("Forfait 4", 80, 100),
("Forfait 5", 100, 150);

INSERT INTO client (`last_name`, `first_name`, `date_of_birth`, `address`, `zip_code`, `city`, `country`, `credit`)VALUES
("Ka", "Price","1997-11-10","10 rue des fleurs", "44000", "Nantes", "France", 20),
("Brochefor", "Jean", "1963-05-20", "3 rue des iris", "13000", "Marseille", "France", 42),
("Defess", "Ray", "1956-06-04", "118 rue Marcel Paul", "75000", "Paris", "France", 2),
("Bricot", "Judas", "1993-06-01", "10 rue des tocs", "44000", "Nantes","France", 94),
("Diote", "Kelly", "2001-08-29", "12 avenue Paul Bert", "35000", "Rennes", "France", 5);