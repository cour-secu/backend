module.exports = Program = () => {
    const DB = require('../connector')();

    return {
        get: (id, cb) => {
            let sql = "SELECT * FROM program WHERE program_id = ?;";
            DB.exec(sql, [id], cb);
        },
        getAll: (cb) => {
            let sql = "SELECT * FROM program;";
            DB.exec(sql, [], cb);
        },
        add: (obj, cb) => {
            let sql = "INSERT INTO program(`label`, `duration`, `price`) VALUES"
                    + '("?", ?, ?);';
            DB.exec(sql, [obj.label, obj.duration, obj.price], cb);
        },
        update: (obj, cb) => {
            let sql = 'UPDATE program SET `label` = "?", `duration` = ?, `price` = ? WHERE program_id = ?;';
            DB.exec(sql, [obj.label, obj.duration, obj.price, obj.id], cb);
        },
        remove: (id, cb) => {
            let sql = 'DELETE FROM program WHERE program_id = ?;';
            DB.exec(sql, [id], cb);
        }
    }
}
