module.exports = connector = () => {
  const mysql = require('mysql');

  return  {
    exec: (sqlQuery, arrayValues, callback) => {
      const subExec = (subSqlQuery, subArrayValues, subCallback) => {
      const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "root",
        database: "eatcarwash"
      });
      connection.connect(function(errorConnection) {
        if (errorConnection) return subCallback(errorConnection, null);
          console.log("Connected!");
          return connection.query(subSqlQuery, subArrayValues, function (errorQuery, result) {
            if (errorQuery) return subCallback(errorQuery, null);
            return subCallback(null, result);
          });
        });
      };
      subExec(sqlQuery, arrayValues, (err, result) => {
        if (err) return callback(err);
        callback(null, result);
      });
    }
  }
}