DROP DATABASE IF EXISTS eatcarwash;
CREATE DATABASE eatcarwash;
USE eatcarwash;

CREATE TABLE client(
    client_id int(11)NOT NULL AUTO_INCREMENT,
    last_name varchar(100) NOT NULL,
    first_name varchar(100) NOT NULL,
    date_of_birth date NOT NULL,
    address varchar(255) NOT NULL,
    zip_code varchar(20) NOT NULL,
    city varchar(255) NOT NULL,
    country varchar(255) NOT NULL,
    credit decimal(6,2),
    PRIMARY KEY(client_id)
)AUTO_INCREMENT=1;

CREATE TABLE subscription(
    subscription_id int(11)NOT NULL AUTO_INCREMENT,
    label varchar(255) NOT NULL,
    price decimal(6,2) NOT NULL,
    credit decimal(6,2) NOT NULL,
    PRIMARY KEY(subscription_id)
)AUTO_INCREMENT=1;

CREATE TABLE program(
    program_id int(11) NOT NULL AUTO_INCREMENT,
    label varchar(255) NOT NULL,
    duration int(11) NOT NULL,
    price decimal(6,2) NOT NULL,
    PRIMARY KEY(program_id)
)AUTO_INCREMENT=1;

CREATE TABLE hist_program(
    hist_program_id int(11) NOT NULL AUTO_INCREMENT,
    client_id int(11),
    program_id int(11) NOT NULL,
    wash_date datetime NOT NULL,
    wash_price decimal(6,2) NOT NULL,
    payment_method varchar(100) NOT NULL,
    PRIMARY KEY(hist_program_id),
    CONSTRAINT FK_client_pgrm
    FOREIGN KEY (client_id) REFERENCES client(client_id),
    CONSTRAINT FK_program_pgrm
    FOREIGN KEY (program_id) REFERENCES program(program_id)
)AUTO_INCREMENT=1;

CREATE TABLE hist_subscription(
    hist_subscription_id int(11) NOT NULL AUTO_INCREMENT,
    client_id int(11) NOT NULL,
    subscription_id int(11) NOT NULL,
    subscription_date datetime NOT NULL,
    PRIMARY KEY(hist_subscription_id),
    CONSTRAINT FK_client_sub
    FOREIGN KEY (client_id) REFERENCES client(client_id),
    CONSTRAINT FK_subscription_sub
    FOREIGN KEY (subscription_id) REFERENCES subscription(subscription_id)
)AUTO_INCREMENT=1;