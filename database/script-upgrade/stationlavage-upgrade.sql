ALTER TABLE hist_program
MODIFY wash_date datetime;

ALTER TABLE hist_subscription
MODIFY subscription_date datetime;